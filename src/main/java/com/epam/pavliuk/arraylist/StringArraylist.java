package com.epam.pavliuk.arraylist;

public class StringArraylist {

  private static final Integer INITIAL_CAPACITY = 10;
  private Integer size = 0;
  private String[] array;

  public StringArraylist() {
    array = new String[INITIAL_CAPACITY];
  }

  public StringArraylist(Integer size) {
    array = new String[size];
  }

  public void add(String element) {
    if (checkIfEnoughSpace()) {
      increaseArraySize();
    }
    array[size] = element;
    size++;
  }

  public String get(Integer index) {
    if (index >= size) {
      throw new IndexOutOfBoundsException();
    }
    return array[index];
  }

  private boolean checkIfEnoughSpace() {
    return size + 1 > array.length;
  }

  private void increaseArraySize() {
    int oldSize = array.length;
    String[] newArr = new String[oldSize * 3 / 2];
    for (int i = 0; i < array.length; i++) {
      newArr[i] = array[i];
    }
    array = newArr;
  }

  public Integer getSize() {
    return size;
  }

  public String[] getArray() {
    return array;
  }
}
