package com.epam.pavliuk.comparison;

public class TwoStrings implements Comparable<TwoStrings> {

  private String left;
  private String right;

  public TwoStrings(String left, String right) {
    this.left = left;
    this.right = right;
  }

  public int compareTo(TwoStrings twoStrings) {
    return this.left.compareTo(twoStrings.getLeft());
  }

  public String getLeft() {
    return left;
  }

  public void setLeft(String left) {
    this.left = left;
  }

  public String getRight() {
    return right;
  }

  public void setRight(String right) {
    this.right = right;
  }

  @Override
  public String toString() {
    return "n='" + left + ", s=" + right;
  }
}
