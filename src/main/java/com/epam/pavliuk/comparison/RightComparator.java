package com.epam.pavliuk.comparison;

import java.util.Comparator;

public class RightComparator implements Comparator<TwoStrings> {

  public RightComparator() {
  }

  public int compare(TwoStrings twoStrings1, TwoStrings twoStrings2) {
    return twoStrings1.getRight().compareTo(twoStrings2.getRight());
  }
}
