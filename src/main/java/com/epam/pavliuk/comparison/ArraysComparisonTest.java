package com.epam.pavliuk.comparison;

import com.epam.pavliuk.service.StringGenerator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ArraysComparisonTest {

  private List<TwoStrings> twoStringsArrayList = new ArrayList<TwoStrings>(8);
  private TwoStrings[] twoStrings = new TwoStrings[8];

  public ArraysComparisonTest() {
    buildArrays();
  }

  public void comparableSort() {
    Arrays.sort(twoStrings);
    Collections.sort(twoStringsArrayList);
  }

  public void comparatorSort() {
    Arrays.sort(twoStrings, new RightComparator());
    Collections.sort(twoStringsArrayList, new RightComparator());
  }

  private Integer binarySearchUsingComparator(Integer tryIndex) {
    TwoStrings searched = twoStringsArrayList.get(tryIndex);
    return Collections.binarySearch(twoStringsArrayList, searched, new RightComparator());
  }

  public void buildArrays() {
    StringGenerator stringGenerator = new StringGenerator();
    twoStringsArrayList.clear();
    for (int i = 0; i < twoStrings.length; i++) {
      TwoStrings ns = new TwoStrings(stringGenerator.generateString(),
          stringGenerator.generateString());
      twoStringsArrayList.add(ns);
      twoStrings[i] = ns;
    }
  }

  public void printBinarySearchedElement(Integer tryIndex) {
    System.out.println(twoStringsArrayList.get(binarySearchUsingComparator(tryIndex)));
  }

  public void printArrays() {
    System.out.println("Array: ");
    for (TwoStrings twoStrings : twoStrings) {
      System.out.println(twoStrings.toString());
    }
    System.out.println("\nArrayList: ");
    for (TwoStrings twoStrings : twoStringsArrayList) {
      System.out.println(twoStrings.toString());
    }
  }
}
