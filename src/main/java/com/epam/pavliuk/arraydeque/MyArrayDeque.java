package com.epam.pavliuk.arraydeque;

public class MyArrayDeque<E> {

  private Object[] elements;
  private Integer size;
  private E head;
  private E tail;
  private static final Integer INITIAL_CAPACITY = 8;

  public MyArrayDeque() {
    this.elements = new Object[INITIAL_CAPACITY];
    size = 0;
  }

  public MyArrayDeque(Integer capacity) {
    this.elements = new Object[capacity];
    size = 0;
  }

  public void addFirst(E el) {
    if (addIfEmpty(el)) {
      return;
    }
    if (!isEnoughSpace()) {
      doubleCapacity();
    }
    System.arraycopy(elements, 0, elements, 1, size);
    elements[0] = el;
    head = el;
    size++;
  }

  public void addLast(E el) {
    if (addIfEmpty(el)) {
      return;
    }
    if (!isEnoughSpace()) {
      doubleCapacity();
    }
    elements[size++] = el;
    tail = el;
  }

  public E getFirst() {
    return head;
  }

  public E getLast() {
    return tail;
  }

  private boolean addIfEmpty(E el) {
    if (elements[0] == null) {
      elements[0] = el;
      size++;
      head = el;
      tail = el;
      return true;
    }
    return false;
  }

  private boolean isEnoughSpace() {
    if (size + 1 >= elements.length) {
      return false;
    }
    return true;
  }

  private void doubleCapacity() {
    Object[] newElements = new Object[elements.length * 2];
    System.arraycopy(elements, 0, newElements, 0, elements.length);
    elements = newElements;
  }

  public void removeFirst() {
    head = (E) elements[1];
    System.arraycopy(elements, 1, elements, 0, size);
  }

  public void removeLast() {
    tail = (E) elements[size - 2];
    elements[size - 1] = null;
  }

}
