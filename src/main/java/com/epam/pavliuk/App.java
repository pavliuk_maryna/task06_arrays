package com.epam.pavliuk;


import com.epam.pavliuk.arraylist.StringArraylist;
import com.epam.pavliuk.comparison.ArraysComparisonTest;
import java.util.ArrayList;
import java.util.List;

public class App {

  public static void main(String[] args) {
    App app = new App();
    app.compareArraylists(1000000);

    ArraysComparisonTest arraysComparisonTest = new ArraysComparisonTest();
    System.out.println("Arrays are: ");
    arraysComparisonTest.printArrays();
    System.out.println("\nAfter left sort using Comparable: ");
    arraysComparisonTest.comparableSort();
    arraysComparisonTest.printArrays();
    System.out.println("\nAfter right sort using Comparator: ");
    arraysComparisonTest.comparatorSort();
    arraysComparisonTest.printArrays();
    System.out.println("\nBinary search 2-th element of Arraylist via Comparator: ");
    arraysComparisonTest.printBinarySearchedElement(1);
  }

  public void compareArraylists(Integer elementQuantity) {
    StringArraylist stringArraylist = new StringArraylist();
    List<String> defaultStringArraylist = new ArrayList<String>();
    Long currentTimeMilisec = System.currentTimeMillis();
    for (int i = 0; i < elementQuantity; i++) {
      stringArraylist.add(String.valueOf(i));
    }
    Long timeOfStringArraylist = System.currentTimeMillis() - currentTimeMilisec;
    for (int i = 0; i < elementQuantity; i++) {
      defaultStringArraylist.add(String.valueOf(i));
    }
    Long timeOfDefaultStringArraylist = System.currentTimeMillis() - currentTimeMilisec;
    System.out.println(
        "My arraylist added " + elementQuantity + " elements in " + timeOfStringArraylist
            + " millisecond's");
    System.out.println(
        "Java arraylist added " + elementQuantity + " elements in " + timeOfDefaultStringArraylist
            + " millisecond's");
    System.out.println("Difference is " + (timeOfDefaultStringArraylist - timeOfStringArraylist)
        + " milisecond's");
  }

}
