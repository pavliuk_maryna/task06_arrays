package com.epam.pavliuk.service;

public class StringGenerator {

  private final static String alphabet = "abcdefghijklmnopqrstuvwxyz";

  public StringGenerator() {
  }

  public String generateString() {
    String string = "";
    RandomService randomService = new RandomService();
    for (int i = 0; i < 5; i++) {
      string += String
          .valueOf(alphabet.charAt(randomService.getRandomInRange(0, alphabet.length())));
    }
    return string;
  }
}
