package com.epam.pavliuk.service;

import java.util.Random;

public class RandomService {

  Random rand = new Random();

  public Integer getRandomInRange(Integer leftRange, Integer rightRange) {
    return rand.nextInt((rightRange - leftRange)) + leftRange;
  }
}
